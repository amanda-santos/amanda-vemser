import React from 'react';
import { Link } from 'react-router-dom';
import '../css/reset.css';

export default function Menu(){
        return (
            <React.Fragment>
                <input id="mobile-menu" type="checkbox" />
                <ul class="clearfix">
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/sobre">Sobre</Link>
                    </li>
                    <li>
                        <Link to="/servicos">Serviços</Link>
                    </li>
                    <li>
                        <Link to="/contato">Contato</Link>
                    </li>
                </ul>
            </React.Fragment>
        )
}