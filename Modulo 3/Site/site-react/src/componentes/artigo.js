import React from 'react';

const artigo  = props => {
    return(
        <React.Fragment>
            <h2>{props.titulo}</h2>
            <p>{props.texto}</p>
        </React.Fragment>
    )
} 

export default artigo;