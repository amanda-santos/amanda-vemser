import React from 'react';
import Cabecalho from './cabecalho';
import Card from './card';
import Footer from './footer';
import Button from './button';
import Artigo from './artigo'
import '../css/grid.css';
import '../css/reset.css';
import '../css/general.css';
import '../css/banner.css';
import '../css/box.css';
import '../css/buttons.css';
import '../css/footer.css';
import '../css/about-us.css';
import Imagem1 from '../img/fotoDBC.jpg'


export function linhasSobre() {
    return (
        <section class="container about-us">
            <div class="row">
                <article class="col col-sm col-12">
                    <img src={Imagem1}/>
                    <Artigo titulo="Titulo 1" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque."/>
                </article>
            </div>
            <div class="row">
                <article class="col col-12">
                    <img src={Imagem1}/>
                    <Artigo titulo="Titulo 1" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque."/>
                </article>
            </div>
            <div class="row">
                <article class="col col-12">
                    <img src={Imagem1}/>
                    <Artigo titulo="Titulo 1" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque."/>
                </article>
            </div>
            <div class="row">
                <article class="col col-12">
                    <img src={Imagem1}/>
                    <Artigo titulo="Titulo 1" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque."/>
                </article>
            </div>
        </section>
    )
}

export default function AboutUi(){
    return(
        <React.Fragment>
            <Cabecalho/>
            {linhasSobre()}
            <Footer/>
        </React.Fragment>
    )
}
