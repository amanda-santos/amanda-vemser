import React from 'react';
import Cabecalho from './cabecalho';
import Footer from './footer';
import Artigo from './artigo';
import Input from './input';
import '../css/grid.css';
import '../css/reset.css';
import '../css/general.css';
import '../css/box.css';
import '../css/buttons.css';
import '../css/footer.css';
import '../css/work.css';
import '../css/contact.css';

export function formulario(){
    return(
        <section class="container">
        <div class="row">
                <div class="col col-12 col-lg-6">
                    <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    <form class="clearfix">
                        <Input class="field" type="text" placeholder="Nome"/>
                        <Input class="field" type="text" placeholder="E-mail"/>
                        <Input class="field" type="text" placeholder="Assunto"/>
                        <textarea class="field" placeholder="Mensagem"></textarea>
                        <Input class="button button-green button-right" type="submit" value="Enviar"/>
                    </form>
                </div>
            </div>
            <div class="map-container">
                <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760855!2d-51.17087028474702!3d-30.01619288189262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576870098547!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe>
            </div>
    </section>
    )
}

export default function contatoUi(){
    return(
        <React.Fragment>
            <Cabecalho/>
            {formulario()}
            <Footer/>
        </React.Fragment>
    )
}
