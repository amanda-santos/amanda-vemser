import React from 'react';
import '../css/buttons.css';
import '../css/work.css';


    const input = props => {
        return(
            <React.Fragment>
            <input class={props.class} type={props.type} placeholder={props.placeholder} value={props.value}/>
            </React.Fragment>
        )
    } 
export default input;