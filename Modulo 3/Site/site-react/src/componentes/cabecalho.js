import React from 'react';
import Menu from './menu';
import '../css/reset.css';
import '../css/header.css';
import logoDBC from '../img/logo-dbc-topo.png';

export default function cabecalho(){
    return (
        <React.Fragment>
            <header class="main-header">
                <nav class="container clearfix">
                    <a class="logo" href="index.html" title="Voltar à home">
                        <img src={logoDBC} alt="DBC Company" />
                    </a>
                    <label class="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <Menu />
                </nav>
            </header>
        </React.Fragment>
    )
}
