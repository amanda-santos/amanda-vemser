import React from 'react';
import Cabecalho from './cabecalho';
import Card from './card';
import Footer from './footer';
import Button from './button';
import Artigo from './artigo'
import '../css/grid.css';
import '../css/buttons.css';
import '../css/reset.css';
import '../css/general.css';
import '../css/banner.css';
import '../css/box.css';
import '../css/footer.css';
import '../css/work.css';
import Imagem1 from '../img/Linhas-de-Serviço-Agil.png'
import Imagem2 from '../img/Linhas-de-Serviço-DBC_Smartsourcing.png'
import Imagem3 from '../img/Linhas-de-Serviço-DBC_Software-Builder.png'

export function conjuntoDeArtigos(){
    return(
        <section class="container">
            <div class="row">
                <div class="col col-12 col-md-4">
                    <article class="box">
                        <img src={Imagem1}/>
                        <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    </article>
                </div>
                <div class="col col-12 col-md-4">
                    <article class="box">
                        <img src={Imagem2}/>
                        <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    </article>
                </div>
                <div class="col col-12 col-md-4">
                    <article class="box">
                        <img src={Imagem3}/>
                        <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col col-12 col-md-4">
                    <article class="box">
                        <img src={Imagem1}/>
                        <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    </article>
                </div>
                <div class="col col-12 col-md-4">
                    <article class="box">
                        <img src={Imagem2}/>
                        <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    </article>
                </div>
                <div class="col col-12 col-md-4">
                    <article class="box">
                        <img src={Imagem3}/>
                        <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    </article>
                </div>
            </div>
        </section>
    )
}

export function article() {
    return (
        <section class="container work">
            <div class="row">
                <article class="col col-12">
                    <Artigo titulo="Titulo 1" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                    <Button class="button button-blue">Saiba Mais</Button>
                </article>
            </div>
        </section>
    )
}

export default function servicosUi(){
    return(
        <React.Fragment>
            <Cabecalho/>
            {conjuntoDeArtigos()}
            {article()}
            <Footer/>
        </React.Fragment>
    )
}
