import React from 'react';
import Cabecalho from './cabecalho';
import Card from './card';
import Footer from './footer';
import Button from './button';
import Artigo from './artigo'
import '../css/grid.css';
import '../css/reset.css';
import '../css/general.css';
import '../css/banner.css';
import '../css/box.css';
import '../css/buttons.css';
import '../css/footer.css';
import Imagem1 from '../img/Linhas-de-Serviço-Agil.png'
import Imagem2 from '../img/Linhas-de-Serviço-DBC_Smartsourcing.png'
import Imagem3 from '../img/Linhas-de-Serviço-DBC_Software-Builder.png'
import Imagem4 from '../img/Linhas-de-Serviço-DBC_Sustain.png'

export function banner() {
    return (
        <section class="main-banner">
            <article>
            <Artigo titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
            <Button class="button button-big button-outline" conteudo="Saiba mais"/>
            </article>
        </section>
    );
}

export function articles() {
    return (
        <section class="container">
            <div class="row">
                <article class="col col-12 col-md-7 col-lg-7">
                    <Artigo titulo="Titulo 1" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus.
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                </article>
                <article class="col col-12 col-md-5 col-lg-3">
                    <Artigo titulo="Titulo 2" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus."/>
                </article>
            </div>
        </section>
    )
}

export function conjuntoDeCards(){
    return(
        <section class="container">
            <div class="row">
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <Card imagem={Imagem1} titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus." 
                        class="button button-blue" conteudo="Saiba Mais"/>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <Card imagem={Imagem2} titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus." 
                        class="button button-green" conteudo="Saiba Mais"/>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <Card imagem={Imagem3} titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus." 
                        class="button button-blue" conteudo="Saiba Mais"/>
                    </article>
                </div>
                <div class="col col-12 col-md-6 col-lg-3">
                    <article class="box">
                        <Card imagem={Imagem4} titulo="Titulo" texto="Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus." 
                        class="button button-green" conteudo="Saiba Mais"/>
                    </article>
                </div>
            </div>
        </section>
    )
}

export default function HomeUi(){
    return(
        <React.Fragment>
            <Cabecalho/>
            {banner()}
            {articles()}
            {conjuntoDeCards()}
            <Card />
            <Footer/>
        </React.Fragment>
    )
}
