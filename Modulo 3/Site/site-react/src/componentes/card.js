import React from 'react';
import Button from './button';
import Artigo from './artigo'
import '../css/grid.css';
import '../css/general.css';
import '../css/box.css';
import '../css/buttons.css';

const card = props => {
    return (
        <React.Fragment>
            <img src={props.imagem}/>
            <Artigo titulo={props.titulo} texto={props.texto}/>
            <Button class={props.class} conteudo={props.conteudo}/>
        </React.Fragment>
    )
}
export default card;