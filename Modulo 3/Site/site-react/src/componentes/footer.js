import React from 'react';
import Menu from './menu';
import '../css/reset.css';
import '../css/header.css';

export default function footer(){
    return(
        <footer class="main-footer">
            <div class="container">
                <nav>
                    <Menu/>
                </nav>
                <p>
                    &copy; Copyright DBC Company - 2019
                </p>
            </div>
        </footer>
    )
}




