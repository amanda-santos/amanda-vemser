import React, {Component} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HomeUi from './componentes/homeUI';
import AboutUi from './componentes/aboutUi';
import ServicosUi from './componentes/servicosUi';
import ContatoUi from './componentes/contatoUi';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ HomeUi } />
        <Route path="/sobre" component={ AboutUi } />
        <Route path="/servicos" component={ ServicosUi } />
        <Route path="/contato" component={ ContatoUi } />
      </Router>
    );
  }
}
