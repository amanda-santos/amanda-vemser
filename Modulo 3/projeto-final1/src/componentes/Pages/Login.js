import React, {Component} from 'react';
import axios from 'axios';
import Api from '../../services/Api';
import Input from '../input';
import Botao from '../botao';
import '../../css/grid.css';
import '../../css/input.css';
import '../../css/buttons.css';
import '../../css/reset.css';

export default class Login extends Component{
    constructor(){
        super()
        this.api = new Api();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        this.email = evt.target.email.value;
        this.senha = evt.target.senha.value;
        axios.post(api.login(email, senha))
            .then((res) => {
                localStorage.setItem('token', res.data.token);
                this.props.history.push('/home')                
            }).catch((err) => {
                console.log(err);
                this.props.history.push('/')                
            })
    }

    render(){
        return(
            <form className="login-header" onSubmit={this.handleSubmit}>
                <div className="container">
                    <h1 className="logo-orange">Banco Digital Fake</h1>
                    <Input class="field" id="email" name="email" placeholder="Email" type="text"/>
                    <Input class="field" id="senha" name="senha" placeholder="Senha" type="password"/>
                    <Botao class="button button-orange" descricao="Entrar"/>
                </div>
            </form>
        )
    }
}
