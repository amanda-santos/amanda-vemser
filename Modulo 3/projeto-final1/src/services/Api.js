import axios from 'axios';

export const token = "banco-vemser-api-fake";
export const setToken = token => { localStorage.setItem( 'token', token) }
export const getToken = () => localStorage.getItem('token');
export const IsAuthenticated =  () => localStorage.getItem('token') != null;
export const deslogar = () => {
    localStorage.removeItem('token')
}

export default class Api {

    constructor() {
      this.request = axios.create({
        baseURL: "http://localhost:1337"
      })
      
      this.request.interceptors.request.use( 
        config => {
            const token = getToken();
            if (token) {
            config.headers.Authorization = token;
            }
            return config;
        })
    }
    //https://gist.github.com/ModPhoenix/f1070f1696faeae52edf6ee616d0c1eb
  
    getAgencias = () => this.request.get('/agencias', { headers: { 'Authorization': getToken() } } );
  
    getClientes = () => this.request.get('/clientes', { headers: { 'Authorization': getToken() } } );
  
    getCliente = (id) => this.request.get(`/cliente/${id}`, { headers: { 'Authorization': getToken() } } );
  
    getAgencia = (id) => this.request.get(`/agencia/${id}`, { headers: { Authorization: getToken() } } );
  
    getTiposConta = () => this.request.get('/tipoContas', { headers: { 'Authorization': getToken() } } );
  
    getTipoConta = (id) => this.request.get(`/tiposConta/${id}`, { headers: { 'Authorization': getToken() } } );
  
    getContaClientes = () => this.request.get('/conta/clientes/', { headers: { 'Authorization': getToken() } } );
  
    getContaCliente = (id) => this.request.get(`/conta/cliente/${id}`, { headers: { 'Authorization': getToken() } } );

    login = (email, senha) => this.request.post("/login", { "email": email, "senha":senha });
}