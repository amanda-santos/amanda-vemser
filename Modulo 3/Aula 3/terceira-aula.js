function criarSanduiche(pao, recheio, queijo, salada){
    console.log(`
    Seu sanduíche tem o pão ${pao} 
    com recheio de ${recheio} 
    e queijo ${queijo}
    e com salada de ${salada}`)
}

const ingredientes = ['3 queijos','Frango', 'Cheddar', 'Tomate e Alface'];

//chamado em 40 lugares
//console.log(criarSanduiche(...ingredientes));

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

//receberValoresIndefinidos(1, 2, 3, 4, 5, 6);

//console.log([..."Marcos"])

//Window or Document
let inputTeste = document.getElementById('campoTeste');
inputTeste.addEventListener('blur', () => {
    alert("Obrigado")
});