function validaForm (){
    if(document.getElementById("inputNome").value.length < 1){
        alert('Por favor, preencha o campo nome');
        document.getElementById("inputNome").focus();
        return false
    }
    if(document.getElementById("inputEmail").value.length < 1){
        alert('Por favor, preencha o campo Email');
        document.getElementById("inputEmail").focus();
        return false
    }
    if(document.getElementById("inputTelefone").value.length < 1){
        alert('Por favor, preencha o campo telefone');
        document.getElementById("inputTelefone").focus();
        return false
    }
    if(document.getElementById("inputAssunto").value.length < 1){
        alert('Por favor, preencha o campo assunto');
        document.getElementById("inputAssunto").focus();
        return false
    }
    
}