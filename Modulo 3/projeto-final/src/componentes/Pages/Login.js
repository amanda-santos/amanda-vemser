import React, { Component } from 'react';
import Input from '../Elementos/Input';
import Botao from '../Elementos/botao';
import Api from '../../Api';
import '../../css/App.css';
import './login.css';
import '../../css/buttons.css';

export default class Login extends Component {
    constructor() {
        super()
        this.api = new Api();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        this.email = evt.target.email.value;
        this.senha = evt.target.senha.value;
        this.api.login(this.email, this.senha)
            .then((res) => {
                localStorage.setItem('token', res.data.token);
                this.props.history.push('/home')
            }).catch((err) => {
                console.log(err);
                this.props.history.push('/')
            })
    }

    render() {
        return (
            <form className="App-general" onSubmit={this.handleSubmit}>
                <div className="container">
                    <div className="row offset-4">
                        <h1>Banco Digital Fake</h1>
                    </div>
                    <div className="row">
                        <div className="col-9 offset-3">
                            <Input class="form-control form-login" id="email" name="email" placeholder="Email" type="text" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-9 offset-3">
                            <Input class="form-control form-login" id="senha" name="senha" placeholder="Senha" type="password" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-7 offset-5">
                            <Botao class="button button-outline" descricao="Entrar" />
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}