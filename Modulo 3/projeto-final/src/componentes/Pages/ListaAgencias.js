import React, { Component } from 'react';
import Api from '../../Api';
import { Link } from 'react-router-dom';
import Header from '../Elementos/header';
import Input from '../Elementos/Input';
import Agencia from './Agencia';
import './agencias.css';
import '../../css/App.css';

export default class ListaAgencias extends Component {
  constructor() {
    super()
    this.api = new Api();
    this.state = {
      agencias: [],
    }
  }

  componentDidMount() {
    this.api.getAgencias()
      .then((res) => {
        this.setState({
          agencias: res.data.agencias
        });
        console.log(res.data)
      }).catch((err) => {
        console.log(err);
      })
  }

  pesquisarAgencia = evt => {
    let nome = evt.target.value;
    return this.api.getAgencias()
      .then(value => this.setState({ agencias: value.data.agencias.filter(ag => ag.nome.includes(nome)) }))
      .catch((err) => {
        console.log(err)
      })
  }

  marcarComoDigital = agencia => {
    agencia.isDigital ? agencia.isDigital = false : agencia.isDigital = true;
    console.log(agencia);
  }


  render() {
    const { agencias } = this.state;
    return (
      <React.Fragment>
        <Header />

        <div className="App-general">
          <div className="box">
            <h1>Agências</h1>
            <table className="table tb-media">
              <thead>
                <tr className="link">
                  <th scope="col">#</th>
                  <th scope="col">Nome</th>
                  <th scope="col">Digital?</th>
                </tr>
              </thead>
              <tbody>
                {
                  agencias.map(agencia => {
                    return <tr>
                      <th scope="row">{agencia.id}</th>
                      <th><Link to={{ pathname: `/agencia/${agencia.id}`, component: `${Agencia}` }}>{agencia.nome}</Link></th>
                      <th>
                        <input type="checkbox" onClick={this.marcarComoDigital.bind(this, agencia)}/>
                      </th>
                    </tr>
                  })
                }
              </tbody>
            </table>
          </div>
          <div>
            <Input class="form-control text-center pesquisa" type="text" id="pesquisa" onBlur={this.pesquisarAgencia} placeholder="Pesquise uma agência" />
          </div>
          {/* {this.state.agencias.map(agencia => <p>{agencia.nome}</p>)} */}
        </div>
      </React.Fragment>
    )
  }
}
