import React, { Component } from 'react';
import Api from '../../Api';
import { Link } from 'react-router-dom';
import Header from '../Elementos/header';
import Input from '../Elementos/Input'
import './agencias.css'

export default class TiposDeConta extends Component {
    constructor() {
        super()
        this.api = new Api();
        this.state = {
            tipos: []
        }
    }

    componentDidMount() {
        this.api.getTiposConta()
            .then((res) => {
                this.setState({
                    tipos: res.data.tipos
                });
                console.log(res.data)
            }).catch((err) => {
                console.log(err);
            })
    }

    pesquisarTipoConta = evt => {
        let tipo = evt.target.value;
        return this.api.getTiposConta()
            .then(value => this.setState({ tipos: value.data.tipos.filter(tipodeconta => tipodeconta.tipo.includes(tipo)) }))
            .catch((err) => {
                console.log(err)
            })
    }

    render() {
        const { tipos } = this.state;
        return (
            <React.Fragment>
                <Header />
                <div className="login-header">
                    <h1>Tipos de Conta</h1>
                    <table class="table table-striped table-dark tb-media">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tipo</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                tipos.map(tipo => {
                                    return <tr>
                                        <th scope="row">{tipo.id}</th>
                                        <th><Link to={{ pathname: `/tipoContas/${tipo.id}` }}>{tipo.nome}</Link></th>
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>
                    <div>
                        <Input class="form-control text-center" type="text" id="pesquisa" onBlur={this.pesquisarTipoConta} placeholder="Pesquise um tipo de conta" />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
