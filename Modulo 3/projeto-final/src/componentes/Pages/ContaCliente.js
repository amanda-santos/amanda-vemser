import React, { Component } from 'react';
import Header from '../Elementos/header';
import Api from '../../Api';

export default class ContaCliente extends Component {

    constructor(props) {
        super(props)
        this.api = new Api();
        this.state = {
            cliente: {},
            tipo: {},
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params
        this.MostrarContaCliente(id)

    }

    MostrarContaCliente(id) {
        return this.api.getContaCliente(id - 1)
            .then(value => this.setState({
                cliente: value.data.conta.cliente,
                tipo: value.data.conta.tipo
            }))

    }

    render() {
        const { cliente, tipo } = this.state
        return (
            <div className="login-header">
                <Header />
                <h1>Conta - {cliente.nome}</h1>
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <div className="card text-white bg-danger mt-3">
                                <div className="card-header">
                                    Cliente
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item bg-dark">ID: {cliente.id}</li>
                                    <li className="list-group-item bg-dark">Nome: {cliente.nome}</li>
                                    <li className="list-group-item bg-dark">CPF: {cliente.cpf}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="card text-white bg-danger mt-3">
                                <div className="card-header">
                                    Conta
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item bg-dark">ID: {tipo.id}</li>
                                    <li className="list-group-item bg-dark">Tipo: {tipo.nome}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}