import React, { Component } from 'react';
import Header from '../Elementos/header';
import Api from '../../Api';

export default class Cliente extends Component {

    constructor(props) {
        super(props)
        this.api = new Api();
        this.state = this.state = {
            cliente: {
                agencia: {
                    endereco: {}
                }
            }
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params
        this.MostrarCliente(id)
    }

    MostrarCliente(id) {
        return this.api.getCliente(id - 1)
            .then(value => this.setState({
                cliente: value.data.cliente
            }))

    }

    render() {
        const { cliente } = this.state
        return (
            <div className="login-header">
                <Header />
                <h1>{cliente.nome}</h1>
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <div className="card text-white bg-danger mt-3">
                                <div className="card-header">
                                    Dados
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item bg-dark">ID: {cliente.id}</li>
                                    <li className="list-group-item bg-dark">Nome: {cliente.nome}</li>
                                    <li className="list-group-item bg-dark">CPF: {cliente.cpf}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="card text-white bg-danger mt-3">
                                <div className="card-header">
                                    Agência
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item bg-dark">Logradouro {cliente.agencia.endereco.logradouro}</li>
                                    <li className="list-group-item bg-dark">Nº{cliente.agencia.endereco.numero}</li>
                                    <li className="list-group-item bg-dark">Bairro {cliente.agencia.endereco.bairro}</li>
                                    <li className="list-group-item bg-dark">{cliente.agencia.endereco.cidade} - {cliente.agencia.endereco.uf}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}