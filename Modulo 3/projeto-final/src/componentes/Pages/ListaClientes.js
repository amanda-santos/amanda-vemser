import React, { Component } from 'react';
import Api from '../../Api';
import { Link } from 'react-router-dom';
import Header from '../Elementos/header';
import Input from '../Elementos/Input'
import './agencias.css'

export default class ListaClientes extends Component {
  constructor() {
    super()
    this.api = new Api();
    this.state = {
      clientes: []
    }
  }

  componentDidMount() {
    this.api.getClientes()
      .then((res) => {
        this.setState({
          clientes: res.data.clientes
        });
        console.log(res.data)
      }).catch((err) => {
        console.log(err);
      })
  }

  pesquisarCliente = evt => {
    let nome = evt.target.value;
    return this.api.getClientes()
      .then(value => this.setState({ clientes: value.data.clientes.filter(cl => cl.nome.includes(nome)) }))
      .catch((err) => {
        console.log(err)
      })
  }

  render() {
    const { clientes } = this.state;
    return (
      <React.Fragment>
        <Header />
        <div className="login-header">
          <h1>Clientes</h1>
          <table class="table table-striped table-dark tb-media">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
              </tr>
            </thead>
            <tbody>
              {
                clientes.map(cliente => {
                  return <tr>
                    <th scope="row">{cliente.id}</th>
                    <th><Link to={{ pathname: `/clientes/${cliente.id}` }}>{cliente.nome}</Link></th>
                  </tr>
                })
              }
            </tbody>
          </table>
          <div>
            <Input class="form-control text-center" type="text" id="pesquisa" onBlur={this.pesquisarCliente} placeholder="Pesquise um cliente" />
          </div>
        </div>
      </React.Fragment>
    )
  }
}
