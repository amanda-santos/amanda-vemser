import React, { Component } from 'react';
import Header from '../Elementos/header';
import Api from '../../Api';

export default class ContaCliente extends Component {

    constructor(props) {
        super(props)
        this.api = new Api();
        this.state = {
            tipo: []
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params
        this.MostrarTipoDeConta(id)
    }

    MostrarTipoDeConta(id) {
        return this.api.getTipoConta(id - 1)
            .then(value => this.setState({
                tipo: value.data.tipos
            }))

    }

    render() {
        const { tipo } = this.state
        return (
            <div className="login-header">
                <Header />
                <h1>Tipo - {tipo.nome}</h1>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="card text-white bg-danger mt-3">
                                <div className="card-header">
                                    Cliente
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item bg-dark">ID: {tipo.id}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}