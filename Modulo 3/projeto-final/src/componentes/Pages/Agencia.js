import React, { Component } from 'react';
import Header from '../Elementos/header';
import Api from '../../Api';
import Button from '../Elementos/botao'

export default class Agencia extends Component {

  constructor(props) {
    super(props)
    this.api = new Api();
    this.state = {
      agencia: {},
      endereco: {}
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.MostrarAgencia(id)
  }

  MostrarAgencia(id) {
    return this.api.getAgencia(id - 1)
      .then(value => this.setState({
        agencia: value.data.agencias,
        endereco: value.data.agencias.endereco,
      }))
      console.log(this.agencia)
  }

  //AgenciaIsDigital = 

  render() {
    const { agencia, endereco } = this.state
    return (
      <div className="login-header">
        <Header />
        <h1>Agencia {agencia.nome}</h1>
        <div>
          <h4>ID: {agencia.id} - COD: {agencia.codigo}</h4>
        </div>
        <div className="card text-white bg-danger mt-3">
          <div className="card-header">
            Endereço
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item bg-dark">Logradouro {endereco.logradouro}</li>
            <li className="list-group-item bg-dark">Nº{endereco.numero}</li>
            <li className="list-group-item bg-dark">Bairro {endereco.bairro}</li>
            <li className="list-group-item bg-dark">{endereco.cidade} - {endereco.uf}</li>
            <li className="list-group-item bg-dark">{this.agenciaDigital} </li>
          </ul>
        </div>
      </div>
    );
  }
}