import React, { Component } from 'react';
import Api from '../../Api';
import { Link } from 'react-router-dom';
import Header from '../Elementos/header';
import Input from '../Elementos/Input'
import './agencias.css'

export default class ContasDeClientes extends Component {
  constructor() {
    super()
    this.api = new Api();
    this.state = {
      contas: []
    }
  }

  componentDidMount() {
    this.api.getContaClientes()
      .then((res) => {
        this.setState({
          contas: res.data.cliente_x_conta
        });
        console.log(res.data)
      }).catch((err) => {
        console.log(err);
      })
  }

  pesquisarContaDeClientes = evt => {
    let tipo = evt.target.value;
    return this.api.getContaClientes()
      .then(value => this.setState({ contas: value.data.this.contas.filter(cnt => cnt.tipo.includes(tipo)) }))
      .catch((err) => {
        console.log(err)
      })
  }

  render() {
    const { contas } = this.state;
    return (
      <React.Fragment>
        <Header />
        <div className="login-header">
          <h1>Contas De Clientes</h1>
          <table class="table table-striped table-dark tb-media">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tipo</th>
                <th scope="col">Cliente</th>
              </tr>
            </thead>
            <tbody>
              {
                contas.map(conta => {
                  return <tr>
                    <th scope="col">{conta.id}</th>
                    <th scope="col">{conta.tipo.nome}</th>
                    <th scope="col">{<Link to={{ pathname: `/conta/cliente/${conta.id}` }}>{conta.cliente.nome}</Link>}</th>
                  </tr>
                })
              }
            </tbody>
          </table>
          <div>
            <Input class="form-control text-center" type="text" id="pesquisa" onBlur={this.pesquisarContaDeClientes} placeholder="Pesquise uma conta" />
          </div>
        </div>
      </React.Fragment>
    )
  }
}
