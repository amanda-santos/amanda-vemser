import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../css/App.css'

export default class Home extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="App-general">
                    <div>
                        <li>
                            <Link to={{ pathname: "/Agencias" }}> Agências </Link>
                        </li>
                        <li>
                            <Link to={{ pathname: "/Clientes" }}> Clientes </Link>
                        </li>
                        <li>
                            <Link to={{ pathname: "/TiposDeConta" }}> Tipos de Conta </Link>
                        </li>
                        <li>
                            <Link to={{ pathname: "/contasDeClientes" }}> Conta de Clientes </Link>
                        </li>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}