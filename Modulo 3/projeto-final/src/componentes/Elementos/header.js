import React from 'react';
import Menu from './menu';
import { Link } from 'react-router-dom';
import '../../css/header.css';

export default function cabecalho() {
    return (
        <React.Fragment>
            <header class="main-header">
                <nav class="navbar navbar-light bg-light">
                    <Link class="navbar-brand font-weight-bold logo" href="#">
                        Banco Digital Fake
                    </Link>
                    <label class="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <Menu />
                </nav>
            </header>
        </React.Fragment>
    )
}
