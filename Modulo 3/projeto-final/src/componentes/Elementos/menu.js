import React from 'react';
import { Link } from 'react-router-dom';
import '../../css/reset.css';

export default function Menu(){
        return (
            <React.Fragment>
                <input id="mobile-menu" type="checkbox" />
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <Link className="nav-link active" to="/home">Home</Link>
                    </li>
                    <li>
                        <Link className="nav-link active" to="/agencias">Agências</Link>
                    </li>
                    <li>
                        <Link className="nav-link active" to="/clientes">Clientes</Link>
                    </li>
                    <li>
                        <Link className="nav-link active" to="/tiposDeConta">Tipos De Conta</Link>
                    </li>
                    <li>
                        <Link className="nav-link active" to="/contasDeClientes">Contas de Clientes</Link>
                    </li>
                </ul>
            </React.Fragment>
        )
}