import React, { Component } from 'react';

export default class Input extends Component {
    render() {
        return (
            <React.Fragment>
                <input className={this.props.class} id={this.props.id} type={this.props.type} placeholder={this.props.placeholder} onBlur={this.props.onBlur} />
            </React.Fragment>
        )
    }
}