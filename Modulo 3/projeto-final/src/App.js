import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import IsAuthenticated from './Auth';

import Login from './componentes/Pages/Login';
import Home from './componentes/Pages/Home';
import Agencias from './componentes/Pages/ListaAgencias';
import Clientes from './componentes/Pages/ListaClientes.js';
import TiposDeConta from './componentes/Pages/TiposDeConta';
import TipoDeConta from './componentes/Pages/TipoDeConta';
import ContasDeClientes from './componentes/Pages/ContasDeClientes';
import Agencia from './componentes/Pages/Agencia';
import Cliente from './componentes/Pages/Cliente';
import ContaCliente from './componentes/Pages/ContaCliente';


const PrivateRoute = ( { component: Component, ...rest } ) => (
  <Route { ...rest } render={ props => (
    IsAuthenticated() ? 
      ( <Component { ...props }/> ) :
      ( <Redirect to={ { pathname: '/', state: { from: props.location } } }/> )
  )}/>
)

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Login } />
        <PrivateRoute path="/home" exact component={ Home } />
        <PrivateRoute path="/agencias" exact component={ Agencias } />
        <PrivateRoute path="/clientes" exact component={ Clientes } />
        <PrivateRoute path="/tiposDeConta" exact component={ TiposDeConta } />
        <PrivateRoute path="/contasDeClientes" exact component={ ContasDeClientes } />
        <PrivateRoute path="/agencia/:id" exact component={ Agencia } />
        <PrivateRoute path="/clientes/:id" exact component={ Cliente } />
        <PrivateRoute path="/tipoContas/:id" exact component={ TipoDeConta } />
        <PrivateRoute path="/conta/cliente/:id" exact component={ ContaCliente } />

      </Router>
    );
  }
}