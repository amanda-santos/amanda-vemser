// console.log("Cheguei", "Aqui");

var nomeDaVariavel = "valor";
let nomeDaLet = "ValorLet";
const nomeDaConst = "ValorConst";

var nomeDaVariavel = "Valor3";
nomeDaLet = "ValorLet2";

const nomeDaConst2 = {
    nome: "Marcos",
    idade: 26
};

Object.freeze(nomeDaConst2);

nomeDaConst2.nome = "Marcos Henrique"; 

//console.log(nomeDaConst2);

function nomeDaFuncao(){
//    var nomeDaVariavel = "valor";
    let nomeDaLet = "ValorLet2";
}

//console.log(nomeDaLet);

function somar(){

}

function somar(valor1, valor2 = 1){
    console.log(valor1 + valor2);
}

//somar(3);
//somar(3, 2);

/*console.log(1 + 1);
console.log(1 + "1");
console.log(1 + "1" + 1);
console.log(1 + 1 + "1");
*/

function ondeMoro(cidade){
    //console.log("Eu moro em " + cidade + ". E sou feliz");
    console.log(`Eu moro em ${cidade}. E sou feliz`);

}

//ondeMoro("Gravataí");

function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pessego"
                + "\n";
    let newTexto = `
            Banana 
                Ameixa
                    Goiaba
                        Pessego
                `;

    console.log(texto);
    console.log(newTexto);
}

//fruteira();

let eu = {
    nome: "Marcos",
    idade: 29,
    altura: 1.85
};

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} e ${pessoa.altura} de altura.`);
}

//quemSou(eu);

let funcaoSomarValores = function(a, b){
    return a + b;
}

let add = funcaoSomarValores;
let resultado = add (3, 5);
//console.log(resultado);

//destruct
const { nome:n, idade:i } = eu;
//console.log(nome, idade);
//console.log(n, i);

const array = [1, 3, 4, 8];
const [n1, ,n3, n2, n4 = 18] = array;
//console.log(n1, n2, n3, n4);

function testarPessoa({nome, idade, altura}){
    //console.log(nome, idade, altura);
}

testarPessoa(eu);

let a1 = 42;
let b1 = 15;
let c1 = 99;
let d1 = 109;

/*let aux = a1;
a1 = b1;
b1 = aux;

console.log(a1, b1, c1, d1);

[a1, b1, c1, d1] = [d1, c1, b1, a1];

console.log(a1, b1, c1, d1); */

function calcularCirculo(raio, tipoCalculo){
    if (tipoCalculo == "A"){
        resultado = Math.pow(raio, 2) * Math.PI; 
    } else{
        resultado = 2 * Math.PI * raio; 
    }
        return resultado;
}

//console.log(calcularCirculo(20, "C"));

function naoBissexto(ano){
    let bissexto = "Não é bissexto";
    if(ano % 4 == 0){
        if(ano % 100 != 0){
            bissexto = "É bissexto";
        }
    } 
    else{
        if(ano % 400 == 0){
            bissexto = "É bissexto";
        }
    }
    console.log(bissexto);
}

//naoBissexto(2020);

function somarPares([n0, n1, n2, n3, n4]){
    let soma = n0 + n2 + n4;
    console.log(soma);
}

//somarPares([ 1, 56, 4.34, 6, -2 ]);

function adicionar(n1) {
    let soma = n1;
    function addDentro(n2) {
       return soma + n2;
    }
    return addDentro;
 }
 //console.log( adicionar(3)(4));

 function imprimirBRL(valor){
    let converter = "R$" + (valor).toLocaleString('pt-BR');
    let separar = converter.split(',');
    if(separar[1] > 100){
        let paraDecimal = separar[1] / 10;
        let arredondar = Math.ceil(paraDecimal);
        let final = "R$" + separar[0] + "," + arredondar;
        console.log(final);
    } else {
        if(separar[1] < 10 && separar[1] >= 0){
            let nroMenorQue10 = separar[1] + "0" ;  
            let final = "R$" + separar[0] + "," + nroMenorQue10;
            console.log(final);
        }else{
            console.log(converter);
        }
    }
 }
 imprimirBRL(1555896.84);
 