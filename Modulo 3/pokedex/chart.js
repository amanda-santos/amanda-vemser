/* eslint-disable no-undef */
google.charts.load('current', { packages: ['bar'] });

function drawChart(speed, sd, sa, defense, attack, hp) {
  const data = google.visualization.arrayToDataTable([
    ['Atributos', 'valor'],
    ['HP', hp],
    ['Attack', attack],
    ['Defense', defense],
    ['SpAtk', sa],
    ['SpDef', sd],
    ['Speed', speed],

  ]); const options = {
    annotations: {
      textStyle: {
        fontName: 'Times-Roman',
        fontSize: 18,
        bold: true,
        italic: true,
        // The color of the text.
        color: '#000',
      },
    },

    title: 'Atributos',
    pieSliceText: 'value',
    backgroundColor: { fill: 'transparent' },
    colors: ['#cc0000'],
    legend: { position: 'none' },

    axes: {
      x: {
        0: { side: 'bottom', label: '' },
      },
    },
  }; const chart = new google.charts.Bar(document.getElementById('columnchart_material')); chart.draw(data, google.charts.Bar.convertOptions(options));
}
google.charts.setOnLoadCallback(drawChart);
