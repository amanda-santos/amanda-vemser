package br.com.dbccompany.Cartoes;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.LojaXCredenciador;

public class Main {
	
	public static void main(String[] args) {
		Session session = null;
		org.hibernate.Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Loja Qualquer");
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Amanda");
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			credenciador.setNome("Algum credenciador");
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Master");
			bandeira.setTaxa(0.3);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Bradesco");
			emissor.setTaxa(0.2);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setChip("Sim");
			cartao.setVencimento("25/07/28");
			cartao.setCliente(cliente);
			cartao.setBandeira(bandeira);
			cartao.setEmissor(emissor);
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setDataCompra("12/02/2020");
			lancamento.setDescricao("Vestido");
			lancamento.setValor(60.50);
			lancamento.setCartao(cartao);
			lancamento.setEmissor(emissor);
			lancamento.setLoja(loja);
			
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			List<CredenciadorEntity> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			
			LojaXCredenciador lojaXCredenciador = new LojaXCredenciador();
			lojaXCredenciador.setCredenciador(credenciadores);
			lojaXCredenciador.setLoja(lojas);
			lojaXCredenciador.setTaxa(0.5);
						
			session.save(loja);
			session.save(cliente);
			session.save(credenciador);
			session.save(bandeira);
			session.save(emissor);
			session.save(cartao);
			session.save(loja);
			session.save(lojaXCredenciador);
			
			transaction.commit();
			
			System.out.println("Credenciador: " + lancamento.getValor() * lojaXCredenciador.getTaxa());
			System.out.println("Bandeira: " + lancamento.getValor() * bandeira.getTaxa());
			System.out.println("Emissor: " + lancamento.getValor() * emissor.getTaxa());
			
		} catch (Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}
}