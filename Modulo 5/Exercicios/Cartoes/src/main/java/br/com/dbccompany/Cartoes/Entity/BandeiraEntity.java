package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Inheritance (strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name="BANDEIRA")
@SequenceGenerator(allocationSize = 1, name = "BANDEIRA_SEQ", sequenceName = "BANDEIRA_SEQ")
public class BandeiraEntity {
	
	@Id
	@GeneratedValue(generator = "BANDEIRA_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_BANDEIRA")
	private Integer id;
	
	private String nome;
	
	private Double taxa;
	
	@OneToOne
	private CartaoEntity cartao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getTaxa() {
		return taxa;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}


}
