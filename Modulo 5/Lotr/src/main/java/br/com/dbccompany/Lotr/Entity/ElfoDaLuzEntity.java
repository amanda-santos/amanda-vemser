package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ELFO_DA_LUZ")
public class ElfoDaLuzEntity extends ElfoEntity {
	
	private int qtdAtaques = 0;
	private double qtdVidaGanha= 10;
	
	public ElfoDaLuzEntity() {
		super.setTipo(Tipo.ELFO_DA_LUZ);
		super.setQtdExperienciaPorAtaque(2);
	}
}
