package br.com.dbccompany.Lotr.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ITEM")
@SequenceGenerator(allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
public class ItemEntity {
	
	@Id
	@GeneratedValue(generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id_item;
	private String descricao;
	
	@ManyToMany(mappedBy = "itens")
	private List<InventarioXItem> inventarioXItem = new ArrayList<>();
	
	public Integer getId() {
		return id_item;
	}

	public void setId(Integer id) {
		this.id_item = id;
	}

	public String getNome() {
		return descricao;
	}

	public void setNome(String descricao) {
		this.descricao = descricao;
	}
	
}
