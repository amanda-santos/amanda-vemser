

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf); //exige que um dwarf seja passado
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());// verifica se o valor do objeto eh o mesmo do que
        //esta sendo comparado
    }
    
    @Test
    public void atirar2FlechasZerarFlechasAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void atirar3FlechasDiminuirFlechas2xAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
    }
    
        @Test
    public void atirarFlechasEmDwarfDiminuirVidaDwarf(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(100.0, novoDwarf.getQtdVida(), 0.001);
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void naoCriaElfoNaoIncrementa(){
        assertEquals(0, Elfo.getQtdElfos());
    }
    
    @Test
    public void cria1ElfoContadorUmaVez(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(0, Elfo.getQtdElfos());
    }
    
}
