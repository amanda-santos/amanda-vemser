public class Dwarf extends Personagem {
      
    public Dwarf( String nome ){
        super(nome);
        this.qtdVida = 110.0;
        this.ganharItem( new Item(1, "Escudo") );
        this.dano = 10.0;
    }
    
    public void equiparEscudo(){
        this.dano = 5.0;
    }         
}
