import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    public void ListaDeElfosVerdesComStatusRecemCriado(){
        Exercito novoExercito = new Exercito();
        ElfoVerde novoElfo = new ElfoVerde("Celebron");
        novoExercito.alistar(novoElfo);
        assertEquals(novoElfo, novoExercito.buscar(Status.RECEM_CRIADO).get(0));
    }
    
    @Test
    public void ListaDeElfosComStatusRecemCriado(){
        Exercito novoExercito = new Exercito();
        ElfoVerde novoElfo = new ElfoVerde("Celebron");
        ElfoNoturno outroElfo = new ElfoNoturno("Legolas");
        ElfoVerde algumElfo = new ElfoVerde("Blabla");
        novoExercito.alistar(novoElfo);
        novoExercito.alistar(outroElfo);
        novoExercito.alistar(algumElfo);
        assertEquals(novoElfo, novoExercito.buscar(Status.RECEM_CRIADO).get(0));
        assertEquals(outroElfo, novoExercito.buscar(Status.RECEM_CRIADO).get(1));
        assertEquals(algumElfo, novoExercito.buscar(Status.RECEM_CRIADO).get(2));
    }
}
