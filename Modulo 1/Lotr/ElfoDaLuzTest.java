

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @Test
    public void atacaImparEPerde21Vidas(){
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atacarComEspada(novoDwarf);
        assertEquals(79.0, novoElfo.getQtdVida(), 0.001);
    }
    
    @Test
    public void atacaParEGanha10Vidas(){
        ElfoDaLuz novoElfo = new ElfoDaLuz("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atacarComEspada(novoDwarf);
        novoElfo.atacarComEspada(novoDwarf);
        assertEquals(89.0, novoElfo.getQtdVida(), 0.001);
    }
}
