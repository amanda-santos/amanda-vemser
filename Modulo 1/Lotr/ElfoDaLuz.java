import java.util.*;

public class ElfoDaLuz extends Elfo{
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>( Arrays.asList("Espada de galvorn"));
    private int qtdAtaques;
    private final double QTD_VIDA_GANHA = 10;
    
    {
        qtdAtaques = 0;
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public void perderItem( Item item ){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder){
            super.perderItem(item);
        }
    }
    
    public boolean devePerderVida(){
        return qtdAtaques % 2 == 1;
    }
    
    public void ganharVida(){
        qtdVida += QTD_VIDA_GANHA;
    }
   
    public void atacarComEspada(Dwarf dwarf){
        
        if (this.getStatus() != Status.MORTO){
            dwarf.sofrerDano();
            this.aumentarXP();
            qtdAtaques++;
            if (devePerderVida()){
                this.dano = 21.0;
                this.sofrerDano();
                this.dano = 0.0;
            }else{
                this.ganharVida();
            }
        }
    }
}
