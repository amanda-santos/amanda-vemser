

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoNoturnoTest{
    
    @Test
    public void atirarFlechaGanha3ExperienciaPerde15DeVida(){
        ElfoNoturno novoElfo = new ElfoNoturno("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(3, novoElfo.getExperiencia());
        assertEquals(85.0, novoElfo.getQtdVida(), 0.001);
    
    }
    
}