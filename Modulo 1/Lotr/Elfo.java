public class Elfo extends Personagem{
    
    private int indiceFlecha;
    private static int qtdElfos;
    
    {
         indiceFlecha = 0;
    }
        
    public Elfo( String nome ){
        super(nome);    
        this.qtdVida = 100.0;
        this.inventario.adicionar( new Item(2, "Flecha") );
        this.inventario.adicionar( new Item(1, "Arco") );
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
       /*//nao precisa criar metodo set para experiencia e flecha pois sao
         * //adquiridos por outros metodos.
         * // nao precisa criar metodo set para arco pois so possui um
         * // nao precisa criar get para arco pois ele sempre vai ter um.*/
    
        
    public boolean podeAtirarFlecha(){
        return this.getQtdFlecha() > 0 ? true : false;
    }
    //depois do ? resposta do if
    // depois do : resposta do else
    // se getQtdFlecha() > 0 retorna true, senao false
    
    public void atirarFlecha(Dwarf dwarf){
        if ( podeAtirarFlecha() ){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            //this.experiencia = experiencia + 1;
            this.aumentarXP();            
            dwarf.sofrerDano();
            this.sofrerDano();
        }
    }
}
