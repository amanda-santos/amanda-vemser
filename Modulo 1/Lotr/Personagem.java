public class Personagem
{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double qtdVida;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected double dano;
    
    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia = 0;
        qtdExperienciaPorAtaque = 1;
        dano = 0.0;
    }
    
    public Personagem(String nome){
        this.nome = nome;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome( String nome ){
        this.nome = nome;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public double getQtdVida( ){
        return this.qtdVida;
    }
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public boolean podeSofrerDano(){
        return this.qtdVida > 0;
    }
    
    public void aumentarXP(){
             experiencia = experiencia + qtdExperienciaPorAtaque;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar(item);
    }
    
    public void perderItem( Item item ) {
        this.inventario.remover(item);
    }
      
    
    public void sofrerDano(){
        if ( this.podeSofrerDano() && dano > 0.0){
            this.qtdVida = this.qtdVida >= this.dano ? this.qtdVida - this.dano : 0.0;
            }
        if (this.qtdVida == 0.0){
            this.status = Status.MORTO;
        }else {
            this.status = Status.SOFREU_DANO;
        }

    }
}
