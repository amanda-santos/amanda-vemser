

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest
{
    
    @Test
    public void atirarFlechaGanhar2Xp(){
        ElfoVerde novoElfo = new ElfoVerde("Celebron");
        novoElfo.atirarFlecha(new Dwarf("Balin")); //exige que um dwarf seja passado
        assertEquals(2, novoElfo.getExperiencia());
    }
    
    @Test
    public void ganhaItemComDescricaoValida(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item flechaVidro = new Item(1,"Flecha de vidro");
        novoElfo.ganharItem(flechaVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals(flechaVidro, inventario.obter(2));
    }
    
    @Test
    public void ganhaItemComDescricaoInvalida(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arcoDeMadeira = new Item(1,"Arco de madeira");
        novoElfo.ganharItem(arcoDeMadeira);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Madeira"));
    }
    
    @Test
    public void perdeItemComDescricaoValida(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item flechaVidro = new Item(1,"Flecha de vidro");
        novoElfo.ganharItem(flechaVidro);
        novoElfo.perderItem(flechaVidro);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertNull(inventario.buscar("Flecha de vidro"));
    }
    
    @Test
    public void perdeItemComDescricaoInvalida(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Item arco = new Item(1,"Arco");
        novoElfo.perderItem(arco);
        Inventario inventario = novoElfo.getInventario();
        assertEquals(new Item(2, "Flecha"), inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        
    }
    
    
}
