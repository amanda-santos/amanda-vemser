
public class TradutorParaEspanhol
{
    public String traduzir( String textoEmPortugues) {
        switch(textoEmPortugues){
            case "Sim": 
                return "Yes";
            case "Obrigado":
            case "Obrigada":
                return "Thank you";
            default: 
                return null;
        }
    }
}
