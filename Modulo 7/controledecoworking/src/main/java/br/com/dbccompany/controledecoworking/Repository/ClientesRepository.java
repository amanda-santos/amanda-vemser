package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {

    ClientesEntity findByNome( String nome );
    ClientesEntity findByCpf( String cpf );


}
