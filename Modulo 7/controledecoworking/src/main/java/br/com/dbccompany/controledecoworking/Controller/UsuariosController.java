package br.com.dbccompany.controledecoworking.Controller;

import br.com.dbccompany.controledecoworking.Entity.UsuariosEntity;
import br.com.dbccompany.controledecoworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuariosController {

    @Autowired
    UsuariosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<UsuariosEntity> todosUsuarios(){
        return service.todosUsuarios();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public UsuariosEntity novoCliente( @RequestBody UsuariosEntity usuario ){
        return service.salvar( usuario );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public UsuariosEntity editarCliente( @RequestBody UsuariosEntity usuario, @PathVariable Integer id ){
        return service.editar( usuario, id );
    }

    @GetMapping( value = "/buscarEmail/{ email }")
    @ResponseBody
    public UsuariosEntity buscarPorEmail( @RequestBody String email ){
        return service.buscarPorEmail( email );
    }

    @GetMapping( value = "/buscarNome/{ nome }")
    @ResponseBody
    public UsuariosEntity buscarPorNome( @RequestBody String nome ){
        return service.buscarPorNome( nome );
    }

    @GetMapping( value = "/buscarLogin/{ login }")
    @ResponseBody
    public UsuariosEntity buscarPorLogin( @RequestBody String login ){
        return service.buscarPorLogin( login );
    }

}
