package br.com.dbccompany.controledecoworking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControledecoworkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControledecoworkingApplication.class, args);
	}

}
