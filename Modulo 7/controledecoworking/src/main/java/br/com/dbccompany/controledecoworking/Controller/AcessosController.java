package br.com.dbccompany.controledecoworking.Controller;


import br.com.dbccompany.controledecoworking.Entity.AcessosEntity;
import br.com.dbccompany.controledecoworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<AcessosEntity> todosAcessos(){
        return service.todosAcessoss();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public AcessosEntity novoAcesso( @RequestBody AcessosEntity acesso ){
        return service.salvar( acesso );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public AcessosEntity editarAcesso( @RequestBody AcessosEntity acesso, @PathVariable Integer id ){
        return service.editar( acesso, id );
    }

    @GetMapping( value = "/buscarPorDataDeAcesso")
    @ResponseBody
    public AcessosEntity buscarPorDataDeAcesso( @RequestBody Date data ){
        return service.buscarPorDataDeAcesso( data );
    }

}
