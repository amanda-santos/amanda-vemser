package br.com.dbccompany.controledecoworking.Service;

import br.com.dbccompany.controledecoworking.Entity.ClientesEntity;
import br.com.dbccompany.controledecoworking.Entity.ContatoEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContato;
import br.com.dbccompany.controledecoworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClientesEntity salvar( ClientesEntity cliente ){
        TipoContato email = new TipoContato();
        TipoContato tel = new TipoContato();
        email.setTipo("Email");
        tel.setTipo("Telefone");
        ContatoEntity contato1 = new ContatoEntity();
        ContatoEntity contato2 = new ContatoEntity();
        contato1.setTpContato(email);
        contato2.setTpContato(tel);
        List<ContatoEntity> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);
        return repository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientesEntity editar( ClientesEntity cliente, Integer id ){
        cliente.setId( id );
        return repository.save( cliente );
    }

    public List<ClientesEntity> todosClientes(){
        return (List<ClientesEntity>) repository.findAll();
    }

    public ClientesEntity clienteEspecifico( Integer id ){
        Optional<ClientesEntity> cliente = repository.findById(id);
        return cliente.get();
    }

    public ClientesEntity buscarPorNome( String nome ) {
        return repository.findByNome(nome);
    }

    public ClientesEntity buscarPorCpf( String cpf ){
        return repository.findByCpf( cpf );
    }

}
