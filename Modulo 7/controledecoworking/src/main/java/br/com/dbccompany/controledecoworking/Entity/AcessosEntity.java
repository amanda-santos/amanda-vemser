package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
public class AcessosEntity {

    @Id
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ACESSO")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "FK_ID_CLIENTE")
    private ClientesEntity cliente;

    @ManyToOne
    @JoinColumn(name = "FK_ID_ESPACO")
    private EspacosEntity espaco;

    private Boolean is_Entrada;
    private Date data;
    private Boolean is_Excecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public SaldoClienteId getSaldoCliente() {
//        return saldoCliente;
//    }
//
//    public void setSaldoCliente(SaldoClienteId saldoCliente) {
//        this.saldoCliente = saldoCliente;
//    }

    public Boolean getIs_Entrada() {
        return is_Entrada;
    }

    public void setIs_Entrada(Boolean is_Entrada) {
        this.is_Entrada = is_Entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getIs_Excecao() {
        return is_Excecao;
    }

    public void setIs_Excecao(Boolean is_Excecao) {
        this.is_Excecao = is_Excecao;
    }
}
