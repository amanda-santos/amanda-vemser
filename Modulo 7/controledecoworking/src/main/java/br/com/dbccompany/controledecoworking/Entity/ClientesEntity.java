package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
@Table(name = "CLIENTES")
public class ClientesEntity {

    @Id
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTE")
    private Integer id;

    @Basic(optional = false, fetch = FetchType.EAGER)
    private String nome;

    @Basic(optional = false, fetch = FetchType.EAGER)
    @Column(unique = true)
    private String cpf;

    @Basic(optional = false, fetch = FetchType.EAGER)
    private Date data_nascimento;

    @OneToMany(cascade = CascadeType.ALL)
    @Column( name = "FK_ID_CONTATO")
    private List<ContatoEntity> contatos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
