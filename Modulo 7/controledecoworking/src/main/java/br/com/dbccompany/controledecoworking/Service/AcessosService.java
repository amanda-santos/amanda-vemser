package br.com.dbccompany.controledecoworking.Service;


import br.com.dbccompany.controledecoworking.Entity.AcessosEntity;
import br.com.dbccompany.controledecoworking.Entity.EspacosEntity;
import br.com.dbccompany.controledecoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.controledecoworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public AcessosEntity salvar(AcessosEntity acesso ){
        return repository.save( acesso );
    }

    @Transactional( rollbackFor = Exception.class )
    public AcessosEntity editar( AcessosEntity acesso, Integer id ){
        acesso.setId( id );
        return repository.save( acesso );
    }

    public List<AcessosEntity> todosAcessoss(){
        return (List<AcessosEntity>) repository.findAll();
    }

    public AcessosEntity acessoEspecifico( Integer id ){
        Optional<AcessosEntity> acesso = repository.findById(id);
        return acesso.get();
    }

    public AcessosEntity buscarPorDataDeAcesso( Date data ) {
        return repository.findByData( data );
    }



    public AcessosEntity entrar(AcessosEntity acesso, Date data, SaldoClienteService saldoCliente, SaldoClienteEntity saldoDisponivel, EspacosEntity espaco ){
        //String mensagem;
        if ( saldoCliente.temSaldo( saldoDisponivel, espaco) ){
            acesso.setIs_Entrada( true );
            //mensagem = saldoDisponivel.getQuantidade().parseInt();
            if ( data == null){
                acesso.setData( new Date() );
            } else {
                acesso.setData( data );
            }
        }
        return acesso;
    }

}
