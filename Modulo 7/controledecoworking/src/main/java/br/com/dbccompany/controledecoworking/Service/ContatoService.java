package br.com.dbccompany.controledecoworking.Service;


import br.com.dbccompany.controledecoworking.Entity.ContatoEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContato;
import br.com.dbccompany.controledecoworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ContatoEntity salvar(ContatoEntity contato ){
        return repository.save( contato );
    }

    @Transactional( rollbackFor = Exception.class )
    public ContatoEntity editar( ContatoEntity contato, Integer id ){
        contato.setId( id );
        return repository.save( contato );
    }

    public List<ContatoEntity> todosContatos(){
        return (List<ContatoEntity>) repository.findAll();
    }

    public ContatoEntity contatoEspecifico( Integer id ){
        Optional<ContatoEntity> contato = repository.findById(id);
        return contato.get();
    }

    public ContatoEntity buscarPorTipoContato( TipoContato tipo ) {
        return repository.findByTpContato( tipo );
    }

}
