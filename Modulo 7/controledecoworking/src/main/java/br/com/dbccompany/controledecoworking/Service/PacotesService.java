package br.com.dbccompany.controledecoworking.Service;

import br.com.dbccompany.controledecoworking.Entity.PacotesEntity;
import br.com.dbccompany.controledecoworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PacotesEntity salvar(PacotesEntity pacote ){
        return repository.save( pacote );
    }

    @Transactional( rollbackFor = Exception.class )
    public PacotesEntity editar( PacotesEntity pacote, Integer id ){
        pacote.setId( id );
        return repository.save( pacote );
    }

    public List<PacotesEntity> todosPacotes(){
        return (List<PacotesEntity>) repository.findAll();
    }

    public PacotesEntity pacoteEspecifico( Integer id ){
        Optional<PacotesEntity> pacote = repository.findById(id);
        return pacote.get();
    }

    public PacotesEntity buscarPorValor( Double valor ) {
        return repository.findByValor( valor );
    }
}
