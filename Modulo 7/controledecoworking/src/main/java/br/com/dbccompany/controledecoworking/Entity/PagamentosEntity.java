package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
public class PagamentosEntity {

    @Id
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PAGAMENTO")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "FK_ID_CLIENTES_X_PACOTES")
    private ClientesXPacotes clienteXPacote;

    @OneToOne
    @JoinColumn(name = "FK_ID_CONTRATACAO")
    private ContratacaoEntity contratacao;

    @Enumerated(EnumType.STRING)
    private TipoPagamento tpPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesXPacotes getClienteXPacote() {
        return clienteXPacote;
    }

    public void setClienteXPacote(ClientesXPacotes clienteXPacote) {
        this.clienteXPacote = clienteXPacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTpPagamento() {
        return tpPagamento;
    }

    public void setTpPagamento(TipoPagamento tpPagamento) {
        this.tpPagamento = tpPagamento;
    }
}
