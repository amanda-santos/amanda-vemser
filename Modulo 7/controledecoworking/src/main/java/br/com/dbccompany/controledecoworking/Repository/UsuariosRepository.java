package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.UsuariosEntity;
import org.springframework.data.repository.CrudRepository;

public interface UsuariosRepository extends CrudRepository<UsuariosEntity, Integer> {

    UsuariosEntity findByNome( String nome );
    UsuariosEntity findByEmail( String email );
    UsuariosEntity findByLogin( String login );

}
