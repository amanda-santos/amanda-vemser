package br.com.dbccompany.controledecoworking.Controller;

import br.com.dbccompany.controledecoworking.Entity.EspacosEntity;
import br.com.dbccompany.controledecoworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EspacosEntity> todosEspacos(){
        return service.todosEspacos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacosEntity novoEspaco( @RequestBody EspacosEntity espaco ){
        return service.salvar( espaco );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public EspacosEntity editarEspaco( @RequestBody EspacosEntity espaco, @PathVariable Integer id ){
        return service.editar( espaco, id );
    }

    @GetMapping( value = "/buscarPorQtdPessoas/{ pessoas }")
    @ResponseBody
    public EspacosEntity buscarPorQtdPessoas( @RequestBody Integer pessoas ){
        return service.buscarPorQtdPessoas( pessoas );
    }

    @GetMapping( value = "/buscarNome/{ nome }")
    @ResponseBody
    public EspacosEntity buscarPorNome( @RequestBody String nome ){
        return service.buscarPorNome( nome );
    }

}
