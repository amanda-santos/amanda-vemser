package br.com.dbccompany.controledecoworking.Service;

import br.com.dbccompany.controledecoworking.Entity.EspacosEntity;
import br.com.dbccompany.controledecoworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public EspacosEntity salvar(EspacosEntity espaco ){
        return repository.save( espaco );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacosEntity editar( EspacosEntity espaco, Integer id ){
        espaco.setId( id );
        return repository.save( espaco );
    }

    public List<EspacosEntity> todosEspacos(){
        return (List<EspacosEntity>) repository.findAll();
    }

    public EspacosEntity espacoEspecifico( Integer id ){
        Optional<EspacosEntity> espaco = repository.findById(id);
        return espaco.get();
    }

    public EspacosEntity buscarPorNome( String nome ) {
        return repository.findByNome( nome );
    }

    public EspacosEntity buscarPorQtdPessoas( Integer pessoas ){
        return repository.findByQtdPessoas( pessoas );
    }
}
