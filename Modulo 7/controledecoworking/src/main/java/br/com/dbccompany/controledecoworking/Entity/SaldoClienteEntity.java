package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "SALDO_CLIENTE_SEQ", sequenceName = "SALDO_CLIENTE_SEQ")
public class SaldoClienteEntity {

    @EmbeddedId
    private SaldoClienteId id;
    
    @Basic(optional = false, fetch = FetchType.EAGER)
    private Integer quantidade;

    @Basic(optional = false, fetch = FetchType.EAGER)
    private Date vencimento;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false, fetch = FetchType.EAGER)
    private TipoContratacao tpContratacao;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public TipoContratacao getTpContratacao() {
        return tpContratacao;
    }

    public void setTpContratacao(TipoContratacao tpContratacao) {
        this.tpContratacao = tpContratacao;
    }
}
