package br.com.dbccompany.controledecoworking.Service;

import br.com.dbccompany.controledecoworking.Entity.UsuariosEntity;
import br.com.dbccompany.controledecoworking.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService{

    @Autowired
    private UsuariosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public UsuariosEntity salvar(UsuariosEntity usuario ){
        return repository.save( usuario );
    }

    @Transactional( rollbackFor = Exception.class )
    public UsuariosEntity editar( UsuariosEntity usuario, Integer id ){
        usuario.setId( id );
        return repository.save( usuario );
    }

    public List<UsuariosEntity> todosUsuarios(){
        return (List<UsuariosEntity>) repository.findAll();
    }

    public UsuariosEntity usuarioEspecifico( Integer id ){
        Optional<UsuariosEntity> usuario = repository.findById(id);
        return usuario.get();
    }

    public UsuariosEntity buscarPorNome( String nome ) {
        return repository.findByNome(nome);
    }

    public UsuariosEntity buscarPorEmail( String email ) {
        return repository.findByEmail( email );
    }

    public UsuariosEntity buscarPorLogin( String login ) {
        return repository.findByNome( login );
    }



}
