package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    ContratacaoEntity findByTpContratacao(TipoContratacao tipo);

}
