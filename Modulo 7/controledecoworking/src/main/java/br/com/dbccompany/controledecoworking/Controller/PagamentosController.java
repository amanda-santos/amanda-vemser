package br.com.dbccompany.controledecoworking.Controller;

import br.com.dbccompany.controledecoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.controledecoworking.Entity.PagamentosEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoPagamento;
import br.com.dbccompany.controledecoworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<PagamentosEntity> todosPagamentos(){
        return service.todosPagamentos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public PagamentosEntity novoPagamento( @RequestBody PagamentosEntity pagamento ){
        return service.salvar( pagamento );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public PagamentosEntity editarPacote( @RequestBody PagamentosEntity pagamento, @PathVariable Integer id ){
        return service.editar( pagamento, id );
    }

    @GetMapping( value = "/buscarPorTipoDePagamento/{ tpPagamento }")
    @ResponseBody
    public PagamentosEntity buscarPorTpPagamento(@RequestBody TipoPagamento tpPagamento){
        return service.buscarPorTipoPagamento( tpPagamento );
    }

    @GetMapping( value = "/buscarPorContratacao/{ contratacao }")
    @ResponseBody
    public PagamentosEntity buscarPorContratacao(@RequestBody ContratacaoEntity contratacao ){
        return service.buscarPorContratacao( contratacao );
    }

}
