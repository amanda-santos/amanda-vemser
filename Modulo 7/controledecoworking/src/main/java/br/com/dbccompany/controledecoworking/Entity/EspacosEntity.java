package br.com.dbccompany.controledecoworking.Entity;

import br.com.dbccompany.controledecoworking.Util.Conversor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
public class EspacosEntity {

    @Id
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACO")
    private Integer id;

    @Basic(optional = false, fetch = FetchType.EAGER)
    @Column(unique = true)
    private String nome;

    @Basic(optional = false, fetch = FetchType.EAGER)
    private Integer qtdPessoas;

    @Basic(optional = false, fetch = FetchType.EAGER)
    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        String valorString = Double.toString(this.valor);
        return valorString;
    }

    public void setValor(String valor) {
        Double valorDouble = Conversor.conversorStringParaDouble(valor);
        this.valor = valorDouble;
    }

}
