package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity, Integer> {
    EspacosEntity findByNome( String nome );
    EspacosEntity findByQtdPessoas( Integer pessoas );
}
