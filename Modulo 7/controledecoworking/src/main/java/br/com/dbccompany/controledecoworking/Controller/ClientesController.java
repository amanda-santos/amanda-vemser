package br.com.dbccompany.controledecoworking.Controller;


import br.com.dbccompany.controledecoworking.Entity.ClientesEntity;
import br.com.dbccompany.controledecoworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesEntity> todosClientes(){
        return service.todosClientes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesEntity novoCliente( @RequestBody ClientesEntity cliente ){
        return service.salvar( cliente );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public ClientesEntity editarCliente( @RequestBody ClientesEntity cliente, @PathVariable Integer id ){
        return service.editar( cliente, id );
    }

    @GetMapping( value = "/buscarCpf/{ cpf }")
    @ResponseBody
    public ClientesEntity buscarPorCpf( @RequestBody String cpf ){
        return service.buscarPorCpf( cpf );
    }

    @GetMapping( value = "/buscarNome/{ nome }")
    @ResponseBody
    public ClientesEntity buscarPorNome( @RequestBody String nome ){
        return service.buscarPorNome( nome );
    }

}
