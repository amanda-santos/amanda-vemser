package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.AcessosEntity;
import br.com.dbccompany.controledecoworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {
    AcessosEntity findByData( Date data );
}
