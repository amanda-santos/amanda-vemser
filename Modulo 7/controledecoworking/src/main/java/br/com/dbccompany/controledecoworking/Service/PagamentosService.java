package br.com.dbccompany.controledecoworking.Service;

import br.com.dbccompany.controledecoworking.Entity.*;
import br.com.dbccompany.controledecoworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public PagamentosEntity salvar(PagamentosEntity pagamento ){
        SaldoClienteEntity saldo = new SaldoClienteEntity();
        return repository.save( pagamento );
    }

    @Transactional( rollbackFor = Exception.class )
    public PagamentosEntity editar( PagamentosEntity pagamento, Integer id ){
        pagamento.setId( id );
        return repository.save( pagamento );
    }

    public List<PagamentosEntity> todosPagamentos(){
        return (List<PagamentosEntity>) repository.findAll();
    }

    public PagamentosEntity pagamentoEspecifico( Integer id ){
        Optional<PagamentosEntity> pagamento = repository.findById(id);
        return pagamento.get();
    }

    public PagamentosEntity buscarPorContratacao( ContratacaoEntity contratacao) {
        return repository.findByContratacao( contratacao );
    }

    public PagamentosEntity buscarPorTipoPagamento( TipoPagamento tpPagamento ){
        return repository.findByTpPagamento( tpPagamento );
    }


}
