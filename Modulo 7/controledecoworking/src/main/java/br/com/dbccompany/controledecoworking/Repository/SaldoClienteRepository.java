package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, Integer> {

    SaldoClienteEntity findByVencimento( Date vencimento );
    SaldoClienteEntity findByTpContratacao( TipoContratacao contratacao );

}
