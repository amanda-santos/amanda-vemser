package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.controledecoworking.Entity.PagamentosEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {
    PagamentosEntity findByContratacao( ContratacaoEntity contratacao );
    PagamentosEntity findByTpPagamento( TipoPagamento TpPagamento );
}
