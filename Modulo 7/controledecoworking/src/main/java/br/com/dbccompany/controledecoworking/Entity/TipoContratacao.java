package br.com.dbccompany.controledecoworking.Entity;

public enum TipoContratacao {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
