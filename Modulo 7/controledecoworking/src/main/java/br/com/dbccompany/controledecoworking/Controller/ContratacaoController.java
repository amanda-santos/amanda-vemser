package br.com.dbccompany.controledecoworking.Controller;

import br.com.dbccompany.controledecoworking.Entity.ContratacaoEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContratacao;
import br.com.dbccompany.controledecoworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ContratacaoEntity> todasContratacoes(){
        return service.todasContratacoes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ContratacaoEntity novaContratacao( @RequestBody ContratacaoEntity contratacao ){
        return service.salvar( contratacao );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public ContratacaoEntity editarContratacao( @RequestBody ContratacaoEntity contratacao, @PathVariable Integer id ){
        return service.editar( contratacao, id );
    }

    @GetMapping( value = "/buscarTipoContratacao/{ tpContratacao }")
    @ResponseBody
    public ContratacaoEntity buscarPorTipoContratacao( @RequestBody TipoContratacao tpContratacao ){
        return service.buscarPorTipoContratacao( tpContratacao );
    }

    @PostMapping( value = "/contratar")
    @ResponseBody
    public Double contratar( @RequestBody ContratacaoEntity contratacao ){
        return service.contratar( contratacao );
    }

}
