package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
public class ContratacaoEntity {

    @Id
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTRATACAO")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CLIENTES")
    private ClientesEntity cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_ESPACO")
    private EspacosEntity espaco;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tpContratacao;

    private Integer quantidade;

    private Double desconto;

    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClientesEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTpContratacao() {
        return tpContratacao;
    }

    public void setTpContratacao(TipoContratacao tpContratacao) {
        this.tpContratacao = tpContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }


}
