package br.com.dbccompany.controledecoworking.Repository;

import br.com.dbccompany.controledecoworking.Entity.ClientesEntity;
import br.com.dbccompany.controledecoworking.Entity.ContatoEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {
    ContatoEntity findByTpContato( TipoContato tipo );

}
