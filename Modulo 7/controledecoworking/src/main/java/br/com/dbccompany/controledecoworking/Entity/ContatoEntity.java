package br.com.dbccompany.controledecoworking.Entity;

import br.com.dbccompany.controledecoworking.Util.Conversor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
public class ContatoEntity {

    @Id
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTATO")
    private Integer id;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "ID_TIPO_CONTATO")
    private TipoContato tpContato;

    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContato getTpContato() {
        return tpContato;
    }

    public void setTpContato(TipoContato tpContato) {
        this.tpContato = tpContato;
    }

    public String getValor() {
        String valorString = Double.toString(this.valor);
        return valorString;
    }

    public void setValor(String valor) {
        Double valorDouble = Conversor.conversorStringParaDouble(valor);
        this.valor = valorDouble;
    }
}
