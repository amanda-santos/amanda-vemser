package br.com.dbccompany.controledecoworking.Controller;

import br.com.dbccompany.controledecoworking.Entity.PacotesEntity;
import br.com.dbccompany.controledecoworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<PacotesEntity> todosPacotes(){
        return service.todosPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public PacotesEntity novoPacote( @RequestBody PacotesEntity pacote ){
        return service.salvar( pacote );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public PacotesEntity editarPacote( @RequestBody PacotesEntity pacote, @PathVariable Integer id ){
        return service.editar( pacote, id );
    }

    @GetMapping( value = "/buscarPorValor/{ cpf }")
    @ResponseBody
    public PacotesEntity buscarPorValor( @RequestBody Double valor ){
        return service.buscarPorValor( valor );
    }


}