package br.com.dbccompany.controledecoworking.Service;

import br.com.dbccompany.controledecoworking.Entity.*;
import br.com.dbccompany.controledecoworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteEntity salvar(SaldoClienteEntity saldoCliente ){
        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteEntity editar( SaldoClienteEntity saldoCliente, SaldoClienteId id ){
        saldoCliente.setId( id );
        return repository.save( saldoCliente );
    }

    public List<SaldoClienteEntity> todosSaldosClientes(){
        return (List<SaldoClienteEntity>) repository.findAll();
    }

    public SaldoClienteEntity saldoClienteEspecifico( Integer id ){
        Optional<SaldoClienteEntity> saldoCliente = repository.findById(id);
        return saldoCliente.get();
    }

    public SaldoClienteEntity buscarPorVencimento( Date vencimento ) {
        return repository.findByVencimento( vencimento );
    }

    public SaldoClienteEntity buscarPorTipoContratacao( TipoContratacao tpContratacao ){
        return repository.findByTpContratacao( tpContratacao );
    }

    public Boolean temSaldo( SaldoClienteEntity saldoDisponivel, EspacosEntity espaco ){
        Boolean saldo = true;
        if ( saldoDisponivel.getQuantidade() < espaco.getValor() ){
            saldo = false;
        }
        return saldo;
    }
}
