package br.com.dbccompany.controledecoworking.Entity;

public enum TipoPagamento {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA
}
