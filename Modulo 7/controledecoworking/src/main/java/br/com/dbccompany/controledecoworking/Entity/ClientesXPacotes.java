package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ", sequenceName = "CLIENTES_X_PACOTES_SEQ")
public class ClientesXPacotes {

    @Id
    @GeneratedValue(generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTES_X_PACOTES")
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "id_cliente",
            joinColumns = {
                    @JoinColumn(name = "id_cliente_x_pacote")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_cliente")})
    private List<ClientesEntity> clientes = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "id_pacote",
            joinColumns = {
                    @JoinColumn(name = "id_cliente_x_pacote")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_pacote")})
    private List<PacotesEntity> pacotes = new ArrayList<>();

    private Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<ClientesEntity> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClientesEntity> clientes) {
        this.clientes = clientes;
    }

    public List<PacotesEntity> getPacotes() {
        return pacotes;
    }

    public void setPacotes(List<PacotesEntity> pacotes) {
        this.pacotes = pacotes;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
