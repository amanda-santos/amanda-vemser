package br.com.dbccompany.controledecoworking.Controller;


import br.com.dbccompany.controledecoworking.Entity.ContatoEntity;
import br.com.dbccompany.controledecoworking.Entity.TipoContato;
import br.com.dbccompany.controledecoworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contatos")
public class ContatoController {

    @Autowired
    ContatoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ContatoEntity> todosContatos(){
        return service.todosContatos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ContatoEntity novoContato( @RequestBody ContatoEntity contato ){
        return service.salvar( contato );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public ContatoEntity editarContato( @RequestBody ContatoEntity contato, @PathVariable Integer id ){
        return service.editar( contato, id );
    }

    @GetMapping( value = "/buscarTipoContato/{ tpContato }")
    @ResponseBody
    public ContatoEntity buscarPorTipoContato( @RequestBody TipoContato tpContato ){
        return service.buscarPorTipoContato( tpContato );
    }

}
