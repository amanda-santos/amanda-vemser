package br.com.dbccompany.controledecoworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "ESPACOS_X_PACOTES_SEQ", sequenceName = "ESPACOS_X_PACOTES_SEQ")
public class EspacosXPacotes {

    @Id
    @GeneratedValue(generator = "ESPACOS_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACO_X_PACOTE")
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "id_espaco",
            joinColumns = {
                    @JoinColumn(name = "id_espaco_x_pacote")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_espaco")})
    private List<EspacosEntity> espacos = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "id_pacote",
            joinColumns = {
                    @JoinColumn(name = "id_espaco_x_pacote")},
            inverseJoinColumns = {
                    @JoinColumn(name = "id_pacote")})
    private List<PacotesEntity> pacotes = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private TipoContratacao tPContratacao;

    private Integer quantidade;

    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EspacosEntity> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<EspacosEntity> espacos) {
        this.espacos = espacos;
    }

    public List<PacotesEntity> getPacotes() {
        return pacotes;
    }

    public void setPacotes(List<PacotesEntity> pacotes) {
        this.pacotes = pacotes;
    }

    public TipoContratacao gettPContratacao() {
        return tPContratacao;
    }

    public void settPContratacao(TipoContratacao tPContratacao) {
        this.tPContratacao = tPContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
