package br.com.dbccompany.controledecoworking.Controller;

import br.com.dbccompany.controledecoworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.controledecoworking.Entity.SaldoClienteId;
import br.com.dbccompany.controledecoworking.Entity.TipoContratacao;
import br.com.dbccompany.controledecoworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/saldocliente")
public class SaldoClienteController {

    @Autowired
    SaldoClienteService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<SaldoClienteEntity> todosSaldoCliente(){
        return service.todosSaldoCliente();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public SaldoClienteEntity novoSaldoCliente( @RequestBody SaldoClienteEntity saldoCliente ){
        return service.salvar( saldoCliente );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public SaldoClienteEntity editarPacote( @RequestBody SaldoClienteEntity saldoCliente, @PathVariable SaldoClienteId id ){
        return service.editar( saldoCliente, id );
    }

    @GetMapping( value = "/buscarPorTipoDeContratacao/{ tpContratacao }")
    @ResponseBody
    public SaldoClienteEntity buscarPorTpContratacao(@RequestBody TipoContratacao tpContratacao){
        return service.buscarPorTipoContratacao( tpContratacao );
    }

    @GetMapping( value = "/buscarPorVencimento/{ vencimento }")
    @ResponseBody
    public SaldoClienteEntity buscarPorVencimento(@RequestBody Date vencimento ){
        return service.buscarPorVencimento( vencimento );
    }
}
