package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contas")
public class ContaController {

    @Autowired
    ContaService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Conta> todasContas(){
        return service.todasContas();
    }

    @PostMapping("/novo")
    @ResponseBody
    public Conta novaConta( @RequestBody Conta conta ){
        return service.salvar( conta );
    }

    @PutMapping("/editar")
    @ResponseBody
    public Conta editarConta( @RequestBody Conta conta, @PathVariable Integer id ){
        return service.editar( conta, id );
    }

    @GetMapping("/buscar/{ numero }")
    @ResponseBody
    public Conta buscarPorNumero( @RequestBody Integer numero ){
        return service.buscarPorNumero( numero );
    }

}
