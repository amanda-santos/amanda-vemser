package br.com.dbccompany.bancodigital.Service;


import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContaService {

    @Autowired
    private ContaRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Conta salvar( Conta conta){
        return repository.save( conta );
    }

    @Transactional( rollbackFor = Exception.class)
    public Conta editar( Conta conta, Integer id){
        conta.setId( id );
        return repository.save( conta );
    }

    @Transactional( rollbackFor = Exception.class)
    public List<Conta> todasContas(){
        return (List<Conta>) repository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Conta contaEspecifica( Integer id ){
        Optional<Conta> conta = repository.findById( id );
        return conta.get();
    }

    public Conta buscarPorNumero( Integer numero ){
        return repository.findByNumero( numero );
    }




}
