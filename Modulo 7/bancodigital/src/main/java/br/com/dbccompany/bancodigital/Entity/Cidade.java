package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
public class Cidade {

    @Id
    @GeneratedValue(generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CIDADE")
    private Integer id;

    private String nome;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_ESTADO")
    private Estado estado;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_x_cidade",
        joinColumns = { @JoinColumn(name = "id_cidade")},
        inverseJoinColumns = {@JoinColumn(name = "id_cliente")})
    private List<Cliente> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
