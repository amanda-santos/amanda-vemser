package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Cidade;
import br.com.dbccompany.bancodigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidades" )
public class CidadeController {

    @Autowired
    CidadeService service;


    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Cidade> todasCidades(){
        return service.todasCidades();
    }

    @PostMapping( value =  "/nova")
    @ResponseBody
    public Cidade novaCidade(@RequestBody Cidade cidade){
        return service.salvar(cidade);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Cidade editarCidade(@PathVariable Integer id, @RequestBody Cidade cidade){
        return service.editar(cidade, id);
    }

    @GetMapping( value = "/buscar/{ id }")
    @ResponseBody
    public Cidade buscarCidade(@PathVariable Integer id ){
        return service.cidadeEspecifica( id );
    }

    @GetMapping( value = "/buscarNome/{ nome }")
    @ResponseBody
    public Cidade buscarPorNome(@RequestBody String nome ){
        return service.buscarPorNome( nome );
    }

}











