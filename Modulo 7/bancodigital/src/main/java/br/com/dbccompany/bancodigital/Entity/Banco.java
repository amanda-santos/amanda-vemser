package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name="BANCO")
@SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
public class Banco {

    @Id
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_BANCO")
    private Integer id;

    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
