package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/estados")
public class EstadoController {

    @Autowired
    EstadoService service;

    @GetMapping("/todos")
    @ResponseBody
    public List<Estado> todosEstados() {
        return service.todosEstados();
    }

    @PostMapping("/novo")
    @ResponseBody
    public Estado novoEstado(@RequestBody Estado estado) {
        return service.salvar(estado);
    }

    @PutMapping("/editar")
    @ResponseBody
    public Estado editarEstado(@RequestBody Estado estado, @PathVariable Integer id) {
        return service.editar(estado, id);
    }

    @GetMapping( value = "/buscar/{ id }")
    @ResponseBody
    public Estado buscarEstado(@PathVariable Integer id ){
        return service.estadoEspecifico( id );
    }

    @GetMapping( value = "/buscarNome/{ nome }")
    @ResponseBody
    public Estado buscarPorNome(@RequestBody String nome ){
        return service.buscarPorNome( nome );
    }



}
