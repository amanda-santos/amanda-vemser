package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
public class Agencia {

    @Id
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CIDADE")
    private Integer codigo;

    private String nome;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_BANCO")
    private Banco banco;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CIDADE")
    private Cidade cidade;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
}