package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClienteController {

    @Autowired
    ClienteService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Cliente> todosClientes(){
        return service.todosClientes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Cliente novoCliente( @RequestBody Cliente cliente ){
        return service.salvar( cliente );
    }

    @PutMapping( value = "/editar/{ id }")
    @ResponseBody
    public Cliente editarCliente( @RequestBody Cliente cliente, @PathVariable Integer id ){
        return service.editar( cliente, id );
    }

    @GetMapping( value = "/buscarCpf/{ cpf }")
    @ResponseBody
    public Cliente buscarPorCpf( @RequestBody String cpf ){
        return service.buscarPorCpf( cpf );
    }

    @GetMapping( value = "/buscarNome/{ nome }")
    @ResponseBody
    public Cliente buscarPorNome( @RequestBody String nome ){
        return service.buscarPorNome( nome );
    }



}
