package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Movimentacao;
import br.com.dbccompany.bancodigital.Service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("api/movimentacoes")
public class MovimentacaoController {

    @Autowired
    MovimentacaoService service;

    @GetMapping("/todas")
    @ResponseBody
    public List<Movimentacao> todasMovimentacoes(){
        return service.todasMovimentacoes();
    }


}
