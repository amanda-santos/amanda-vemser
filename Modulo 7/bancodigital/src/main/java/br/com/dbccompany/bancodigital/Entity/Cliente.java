package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
public class Cliente {

    @Id
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTE")
    private Integer id;
    private String cpf;
    private String nome;

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_x_cidade",
            joinColumns = { @JoinColumn(name = "id_cliente")},
            inverseJoinColumns = {@JoinColumn(name = "id_cidade")})
    private List<Conta> contas = new ArrayList<>();

    @ManyToMany( cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_x_cidade",
            joinColumns = { @JoinColumn(name = "id_cliente")},
            inverseJoinColumns = {@JoinColumn(name = "id_cidade")})
    private List<Cidade> cidades = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Conta> getContas() { return contas; }

    public void setContas(List<Conta> contas) { this.contas = contas; }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }
}
