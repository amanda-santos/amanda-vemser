package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
public class Estado {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
    @GeneratedValue( generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_pais")
    private Pais pais;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
}
