package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@SequenceGenerator(allocationSize = 1, name = "MOV_SEQ", sequenceName = "MOV_SEQ")
public class Movimentacao {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "MOV_SEQ", sequenceName = "MOV_SEQ")
    @GeneratedValue( generator = "MOV_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private Double valor;

    @Enumerated(EnumType.STRING)
    private TipoMovimentacao tipoMov;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_conta")
    private Conta conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TipoMovimentacao getTipoMov() {
        return tipoMov;
    }

    public void setTipoMov(TipoMovimentacao tipoMov) {
        this.tipoMov = tipoMov;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
}
