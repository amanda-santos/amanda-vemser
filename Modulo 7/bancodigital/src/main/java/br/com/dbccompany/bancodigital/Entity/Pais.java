package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name="PAIS")
@SequenceGenerator(allocationSize = 1, name = "PAIS_SEQ", sequenceName = "PAIS_SEQ")
public class Pais {
    @Id
    @GeneratedValue(generator = "PAIS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PAIS")
    private Integer id;
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
